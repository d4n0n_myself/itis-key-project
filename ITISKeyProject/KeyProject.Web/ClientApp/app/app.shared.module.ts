import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { NgxPaginationModule } from 'ngx-pagination';  

import { AppComponent } from './components/app/app.component';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { GetKeysComponent } from './components/getKeys/getKeys.component';
import { GetReservationsComponent } from './components/getReservations/getReservations.component';
import { GetJournalComponent } from './components/getJournal/getJournal.component';

@NgModule({
    declarations: [
        AppComponent,
        NavMenuComponent,
        GetKeysComponent,
        GetReservationsComponent,
        GetJournalComponent
    ],
    imports: [
        CommonModule,
        HttpModule,
        FormsModule,
        NgxPaginationModule,
        RouterModule.forRoot([
            { path: '', redirectTo: 'getKeys', pathMatch: 'full' },
            { path: 'getKeys', component: GetKeysComponent },
            { path: 'getReservations', component: GetReservationsComponent },
            { path: 'getJournal', component: GetJournalComponent },
            { path: '**', redirectTo: 'getKeys' }
        ])
    ]
})
export class AppModuleShared {
}
