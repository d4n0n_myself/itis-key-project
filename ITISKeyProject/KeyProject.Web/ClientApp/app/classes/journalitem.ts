﻿import { DatePipe } from "@angular/common";
import { Key } from "./key";

export class JournalItem {
    public ActionTime: DatePipe;
    public roomNumber: string;
    public ActionInfo: string;

    constructor(time: DatePipe, number: string, info: string) {
        this.ActionTime = time;
        this.roomNumber = number;
        this.ActionInfo = info;
    }
}