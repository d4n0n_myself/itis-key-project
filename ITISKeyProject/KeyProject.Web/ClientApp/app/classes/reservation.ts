﻿import { DatePipe } from '@angular/common';

export class Reservation {
	public roomNumber: string;
	public reservationStart: DatePipe;
	public duration: string;

	constructor(roomNumber: string, start: DatePipe, duration: string) {
		this.roomNumber = roomNumber;
		this.reservationStart = start;
		this.duration = duration;
	}
}