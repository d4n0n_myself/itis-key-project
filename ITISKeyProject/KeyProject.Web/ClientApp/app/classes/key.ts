﻿export class Key {
    public roomNumber: string;
    public inStock: boolean;

    constructor(roomNumber: string, stock: boolean) {
        this.roomNumber = roomNumber;
        this.inStock = stock;
    }
}