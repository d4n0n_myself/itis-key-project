﻿import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import { Reservation } from '../classes/reservation';
import { DatePipe } from '@angular/common';

@Injectable()
export class MainService {
    constructor(private _http: Http) { }

    getKeys(): Observable<any> {
		return this._http.get("api/Key/GetKeys");
	}

	addKey(key: string) {
		return this._http.post("api/Key/AddKey", new String(key));
	}

	takeKey(key: string) {
		return this._http.post("api/Key/TakeKey", new String(key));
	}

	returnKey(key: string) {
		return this._http.post("api/Key/ReturnKey", new String(key));
	}

	deleteKey(key: string) {
		return this._http.post("api/Key/DeleteKey", new String(key));
	}

	getReservations(): Observable<any> {
		return this._http.get("api/Reservation/GetReservations");
	}

	getReservationsByDate(date: DatePipe): Observable<any> {
        return this._http.post("api/Reservation/GetReservationsByDate", new String(date));
    }

    addReservation(reservation: Reservation) {
        return this._http.post("api/Reservation/AddReservation", reservation);
	}

	editReservation(existingR: Reservation, newR: Reservation) {
		return this._http.post("api/Reservation/EditReservation", new Array<Reservation>(existingR, newR));
	}

	cancelReservation(reservation: Reservation) {
		return this._http.post("api/Reservation/CancelReservation", reservation);
	}

    getJournal(): Observable<any> {
        return this._http.get("api/Journal/GetJournal");
    }

    getJournalByDate(date: DatePipe): Observable<any> {
        return this._http.post("api/Journal/GetJournalByDate", new String(date));
    }
}