import { Component } from '@angular/core';
import { handler } from '../handlers';
import { MainService } from '../../services/main.service';
import { Key } from '../../classes/key';

@Component({
	selector: 'getKeys',
	templateUrl: './getKeys.component.html',
	styleUrls: ['./getKeys.component.css'],
	providers: [MainService]
})

export class GetKeysComponent {
	public keys: Key[];

	constructor(private _enrollmentService: MainService) { }

	update() {
		this._enrollmentService
			.getKeys()
			.subscribe(data => this.keys = data.json() as Key[],
				error => handler.handleError(error))
	}

	ngOnInit() {
		this.update()
	}

	key = "";

	add() {
		this._enrollmentService
			.addKey(this.key)
			.subscribe(data => this.update(),
			error => {
				if (error.status == 422)
					alert("This key already exists");
				else
					handler.handleError(error);
			})
	}

	take(roomNumber: string) {
		this._enrollmentService
			.takeKey(roomNumber)
			.subscribe(data => this.update(),
			error => {
				if (error.status == 422)
					alert("This key is reserved");
				else
					handler.handleError(error);
			})
	}

	return(roomNumber: string) {
		this._enrollmentService
			.returnKey(roomNumber)
			.subscribe(data => this.update(),
				error => handler.handleError(error))
	}

	delete(roomNumber: string) {
		this._enrollmentService
			.deleteKey(roomNumber)
			.subscribe(data => this.update(),
				error => handler.handleError(error))
    }
}
