﻿import { Component, Inject } from '@angular/core';
import { MainService } from '../../services/main.service';
import { Key } from '../../classes/key'
import { JournalItem } from '../../classes/journalitem';

@Component({
    selector: 'getJournal',
    templateUrl: './getJournal.component.html',
    providers: [MainService]
})

export class GetJournalComponent {

    constructor(private _enrollmentService: MainService) { }

    public journal: JournalItem[];

    p: number = 1;

    ngOnInit() {
        this._enrollmentService
            .getJournal()
            .subscribe(data => {
                this.journal = data.json() as JournalItem[];
                console.log('Yeah', data);
            },
                error => console.error('Error', error));
    }
}
