import { Component, Inject } from '@angular/core';
import { handler } from '../handlers';
import { DatePipe } from '@angular/common';
import { MainService } from '../../services/main.service';
import { Reservation } from '../../classes/reservation';

@Component({
	selector: 'getReservations',
	templateUrl: './getReservations.component.html',
	styleUrls: ['./getReservations.component.css'],
	providers: [MainService]
})

export class GetReservationsComponent {
	public reservations: Reservation[];

	constructor(private _enrollmentService: MainService) { }

	getAll() {
		this.byDate = false;
		this.update();
	}

	oldDate: any = null;
	date = new DatePipe("");
	byDate = false;

	getByDate() {
		this.byDate = true;
		this.update();
	}

	update() {
		if (this.byDate)
			this._enrollmentService
				.getReservationsByDate(this.oldDate == null ? this.date : this.oldDate)
				.subscribe(data => {
					this.reservations = data.json() as Reservation[];
					this.oldDate = this.date;
				},
				error => {
					if (error.status == 400)
						alert("Wrong date");
					else
						handler.handleError(error);
				});
		else
			this._enrollmentService
				.getReservations()
				.subscribe(data => this.reservations = data.json() as Reservation[],
					error => handler.handleError(error));
	}

	ngOnInit() {
		this.getAll()
    }

	oldReservation = new Reservation("", new DatePipe(""), "");
	reservation = new Reservation("", new DatePipe(""), "");
	editing = false;

	submit() {
		if (this.editing)
            this._enrollmentService
                .editReservation(this.oldReservation, this.reservation)
                .subscribe(data => {
                    this.editing = false;
                    this.reservation = new Reservation("", new DatePipe(""), "");
                    this.update();
                },
                    error => {
                        if (error.status == 422)
                            alert("Can't edit reservation")
                        else
                            handler.handleError(error);
                    });
		else
			this._enrollmentService
				.addReservation(this.reservation)
                .subscribe(data => {
                    this.reservation = new Reservation("", new DatePipe(""), "");
                    this.update();
                },
				error => {
					if (error.status == 422)
						alert("Can't reserve")
					else
						handler.handleError(error);
				})
	}

	cancelEditing() {
		this.editing = false;
		this.reservation = new Reservation("", new DatePipe(""), "");
	}

	edit(reservation: Reservation) {
		this.editing = true;
		this.oldReservation = reservation;
		this.reservation = new Reservation(
			reservation.roomNumber,
			reservation.reservationStart,
			reservation.duration);
	}

	cancel(reservation: Reservation) {
		this._enrollmentService
			.cancelReservation(reservation)
			.subscribe(data => this.update(), error => handler.handleError(error))
	}
}
