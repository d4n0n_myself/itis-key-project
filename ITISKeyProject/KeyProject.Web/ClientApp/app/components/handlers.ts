﻿export class Handler {
	public readonly successMessage = "Yeah";
	public readonly errorMessage = "Error";

	getErrorMessage(error: any): string {
		return "Error " + error.status + ": " + error.statusText;
	}

	handleSuccess(data: any) {
		console.log(this.successMessage, data);
	}

	handleError(error: any) {
		console.error(this.errorMessage, error);
		alert(this.getErrorMessage(error));
	}
}

export var handler = new Handler();