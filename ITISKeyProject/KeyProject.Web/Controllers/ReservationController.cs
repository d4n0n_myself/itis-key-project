﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using KeyProject.Core;
using KeyProject.Infrastructure.Interfaces;
using KeyProject.Infrastructure.UnitOfWork;

namespace KeyProject.Web.Controllers
{
	[Produces("application/json")]
	[Route("api/[controller]")]
	public class ReservationController : Controller
	{
		private readonly IReservationRepository _storage;

		public ReservationController(IUnitOfWork unitOfWork)
		{
			_storage = unitOfWork.ReservationRepository;
		}

		[HttpGet("[action]")]
		public IEnumerable<Reservation> GetReservations()
		{
            return _storage.GetReservations().OrderBy(item => item.ReservationStart);
		}

        [HttpPost("[action]")]
        public IActionResult GetReservationsByDate([FromBody] string dateStr)
        {
            if (!DateTime.TryParse(dateStr, out DateTime date))
                return BadRequest(dateStr);

            var reservations = _storage.GetReservationsByDate(date);
            return Ok(reservations);
        }

		[HttpPost("[action]")]
		public IEnumerable<Reservation> GetReservationsByRoom([FromBody] string roomNumber)
		{
            return _storage.GetReservationsByRoom(roomNumber).OrderBy(item => item.ReservationStart);
		}

		[HttpPost("[action]")]
		public IActionResult AddReservation([FromBody] Reservation reservation)
		{
			if (!ModelState.IsValid)
				return BadRequest(ModelState);

			if (!_storage.AddReservation(reservation))
				return StatusCode(422, reservation);
			_storage.Save();

			return Ok(reservation);
		}

        [HttpPost("[action]")]
        public IActionResult EditReservation([FromBody] Reservation[] existingAndNew)
        {
            if (!ModelState.IsValid || existingAndNew.Length != 2 || existingAndNew.Any(x => x == null))
                return BadRequest(ModelState);

            var existingR = existingAndNew[0];
            var newR = existingAndNew[1];

            if (!_storage.EditReservation(existingR.RoomNumber, existingR.ReservationStart, newR))
                return StatusCode(422, newR);
            _storage.Save();

            return Ok(newR);
        }

        [HttpPost("[action]")]
        public IActionResult CancelReservation([FromBody] Reservation reservation)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            
            if (!_storage.CancelReservation(reservation.RoomNumber, reservation.ReservationStart))
                return StatusCode(422, reservation);
            _storage.Save();

            return Ok(reservation);
        }
    }
}