﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using KeyProject.Core;
using KeyProject.Infrastructure.Interfaces;
using KeyProject.Infrastructure.UnitOfWork;
using System;

namespace KeyProject.Web.Controllers
{
	[Produces("application/json")]
	[Route("api/[controller]")]
	public class KeyController : Controller
	{
		private readonly IKeyRepository _storage;
        private readonly IJournalRepository _journal;

		public KeyController(IUnitOfWork unitOfWork)
		{
			_storage = unitOfWork.KeyRepository;
            _journal = unitOfWork.JournalRepository;
		}

		[HttpGet("[action]")]
		public IEnumerable<Key> GetKeys()
		{
            return _storage.GetKeys().OrderBy(item => item.RoomNumber) as IEnumerable<Key>;
		}

		[HttpPost("[action]")]
		public ActionResult AddKey([FromBody] string roomNumber)
		{
			if (!ModelState.IsValid)
				return BadRequest(ModelState);

			if (!_storage.AddKey(roomNumber))
				return StatusCode(422, roomNumber);
			_storage.Save();

			return Ok(roomNumber);
		}

		[HttpPost("[action]")]
		public ActionResult DeleteKey([FromBody] string roomNumber)
		{
			if (!ModelState.IsValid)
				return BadRequest(ModelState);

			if (!_storage.DeleteKey(roomNumber))
				return StatusCode(422, roomNumber);
			_storage.Save();

			return Ok(roomNumber);
		}

		[HttpPost("[action]")]
		public ActionResult TakeKey([FromBody] string roomNumber)
		{
			if (!ModelState.IsValid)
				return BadRequest(ModelState);
			
			if (!_storage.TakeKey(roomNumber))
				return StatusCode(422, roomNumber);
            _journal.AddRecord(_storage.GetKeyByRoomNumber(roomNumber), DateTime.Now, "Taken from stock");
			_storage.Save();

			return Ok(roomNumber);
		}

		[HttpPost("[action]")]
		public ActionResult ReturnKey([FromBody] string roomNumber)
		{
			if (!ModelState.IsValid)
				return BadRequest(ModelState);

			if (!_storage.ReturnKey(roomNumber))
				return StatusCode(422, roomNumber);
            _journal.AddRecord(_storage.GetKeyByRoomNumber(roomNumber), DateTime.Now, "Returned to stock");
            _storage.Save();

			return Ok(roomNumber);
		}
	}
}