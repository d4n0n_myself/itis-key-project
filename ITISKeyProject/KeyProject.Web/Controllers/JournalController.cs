﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using KeyProject.Core;
using KeyProject.Infrastructure.Interfaces;
using KeyProject.Infrastructure.UnitOfWork;
using System;

namespace KeyProject.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class JournalController : Controller
    {
        private readonly IJournalRepository _storage;

        public JournalController(IUnitOfWork unitOfWork)
        {
            _storage = unitOfWork.JournalRepository;
        }

        [HttpGet("[action]")]
        public IEnumerable<JournalItem> GetJournal()
        {
            return _storage.GetJournal().OrderByDescending(x => x.ActionTime);
        }

        [HttpPost("[action]")]
        public IActionResult GetJournalByDate([FromBody] string dateStr)
        {
            if (!DateTime.TryParse(dateStr, out DateTime date))
                return BadRequest(dateStr);

            var journal = _storage.GetJournalByDate(date);
            return Ok(journal);
        }
    }
}
