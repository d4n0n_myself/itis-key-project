﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using KeyProject.Core;
using KeyProject.Infrastructure.Interfaces;
using KeyProject.Infrastructure.UnitOfWork;

namespace KeyProject.Web.Controllers
{
	[Produces("application/json")]
	[Route("api/[controller]")]
	public class IdentityController : Controller
	{
		private readonly IIdentityRepository _storage;

		public IdentityController(IUnitOfWork unitOfWork)
		{
			_storage = unitOfWork.IdentityRepository;
		}

		[HttpPost("[action]")]
		public async Task<IActionResult> AddIdentity([FromBody] Identity identity)
		{
			if (!ModelState.IsValid)
				return BadRequest();

			await _storage.AddIdentity(identity);
			 _storage.Save();

			return Ok(identity);
		}
	}
}