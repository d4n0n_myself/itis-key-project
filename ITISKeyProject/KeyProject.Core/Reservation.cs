﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace KeyProject.Core
{
	public class Reservation : ValueObject<Reservation>
	{
		public string Id { get; set; }
        public string RoomNumber { get; set; }
        public DateTime ReservationStart { get; set; }
        [NotMapped]
        public DateTime ReservationEnd => ReservationStart + Duration;
		public TimeSpan Duration { get; set; }
		[NotMapped]
		public HashSet<int> AllowedUsersIDs { get; set; } = new HashSet<int>();

		public Reservation() { }

		public Reservation(string roomNumber, IEnumerable<int> users, DateTime startTime, TimeSpan duration)
		{
			RoomNumber = roomNumber;
			AllowedUsersIDs = users.ToHashSet();
			ReservationStart = startTime;
			Duration = duration;
		}

		public Reservation(string roomNumber, IEnumerable<int> users, DateTime startTime, DateTime endTime)
            : this(roomNumber, users, startTime, endTime - startTime) { }

		public bool Includes(DateTime dateTime)
		{
			return ReservationStart <= dateTime && dateTime <= ReservationEnd;
		}

		public static bool AreIntersecting(Reservation first, Reservation second)
		{
			return !(first.RoomNumber != second.RoomNumber
					|| second.ReservationEnd < first.ReservationStart
					|| first.ReservationEnd < second.ReservationStart);
		}
	}
}