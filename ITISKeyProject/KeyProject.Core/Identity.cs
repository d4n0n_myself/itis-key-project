﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KeyProject.Core
{
    public class Identity : Entity
    {
        public string FullName { get; private set; }
        public string GroupNumber { get; private set; }

        public Identity(int id, string name) : this(id, name, null) { }

        public Identity() { }
        
        public Identity(int id, string name, string group)
        {
            Id = id;
            FullName = name;
            GroupNumber = group;
        }

        public void ChangeGroupNumber(string newGroupNumber)
        {
            if (newGroupNumber.StartsWith("11-"))
                GroupNumber = newGroupNumber;
        }

        public void ChangeName(string newName)
        {
            FullName = newName;
        }
    }
}
