﻿namespace KeyProject.Core
{
    public class Key : Entity
    {
        public string RoomNumber { get; private set; }
        public bool InStock { get; set; } = true;

        public Key() { }
        
        public Key(string roomNumber)
        {
            RoomNumber = roomNumber;
        }

        public void ChangeNumber(string newNumber)
        {
            RoomNumber = newNumber;
        }

        public override string ToString()
        {
            return RoomNumber;
        }
    }
}
