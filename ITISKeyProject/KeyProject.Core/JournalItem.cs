﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KeyProject.Core
{
    public class JournalItem : ValueObject<JournalItem>
    {
        public Guid Id { get; set; }
        public DateTime ActionTime { get; set; } 
        public string RoomNumber { get; set; }
        public string ActionInfo { get; set; }

        public JournalItem()
        { }

        public JournalItem(DateTime time, Key key, string action)
        {
            ActionTime = time;
            RoomNumber = key.RoomNumber;
            ActionInfo = action;
        }
    }
}
