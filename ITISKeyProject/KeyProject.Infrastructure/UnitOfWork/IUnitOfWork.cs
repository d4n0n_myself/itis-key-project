﻿using KeyProject.Infrastructure.Interfaces;
using System;

namespace KeyProject.Infrastructure.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        IKeyRepository KeyRepository { get; }

        IReservationRepository ReservationRepository { get; }

        IIdentityRepository IdentityRepository { get; }

        IJournalRepository JournalRepository { get; }

        void Save();
    }
}
