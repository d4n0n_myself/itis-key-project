﻿using System;
using KeyProject.Infrastructure.Context;
using KeyProject.Infrastructure.Interfaces;
using KeyProject.Infrastructure.Repos;
using Microsoft.EntityFrameworkCore;

namespace KeyProject.Infrastructure.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private bool isDisposed = false;
        private GeneralContext Context;

        private IKeyRepository keyRepository;
        private IReservationRepository reservationRepository;
        private IIdentityRepository identityRepository;
        private IJournalRepository journalRepository;

        public UnitOfWork(string connectionString)
        {
            var options = new DbContextOptionsBuilder<GeneralContext>();
            options.UseSqlServer(connectionString);
            Context = new GeneralContext(options.Options);
        }

        public IKeyRepository KeyRepository => keyRepository ?? (keyRepository = new KeyRepository(Context));

        public IReservationRepository ReservationRepository => reservationRepository ?? (reservationRepository = new ReservationRepository(Context));

        public IIdentityRepository IdentityRepository => identityRepository ?? (identityRepository = new IdentityRepository(Context));

        public IJournalRepository JournalRepository => journalRepository ?? (journalRepository = new JournalRepository(Context));

        #region IDisposableImplementation
        public virtual void Dispose(bool disposing)
        {
            if (!isDisposed)
            {
                if (disposing)
                    Context.Dispose();
                isDisposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Save()
        {
            Context.SaveChanges();
        }
        #endregion
    }
}
