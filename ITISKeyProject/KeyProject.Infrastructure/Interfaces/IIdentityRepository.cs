﻿using System.Threading.Tasks;
using KeyProject.Core;

namespace KeyProject.Infrastructure.Interfaces
{
    public interface IIdentityRepository
    {
        Task AddIdentity(Identity identity);
        int Save();
    }
}