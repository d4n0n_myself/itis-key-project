﻿using System;
using System.Collections.Generic;
using System.Text;
using KeyProject.Core;

namespace KeyProject.Infrastructure.Interfaces
{
    public interface IJournalRepository
    {
        void AddRecord(Key key, DateTime receivedDateTime, string action);
        IEnumerable<JournalItem> GetJournal();
        IEnumerable<JournalItem> GetJournalByDate(DateTime date);
    }
}
