﻿using System.Collections.Generic;
using System.Threading.Tasks;
using KeyProject.Core;

namespace KeyProject.Infrastructure.Interfaces
{
	public interface IKeyRepository
	{
		IEnumerable<Key> GetKeys();
		Key GetKeyByRoomNumber(string roomNumber);
		bool AddKey(string roomNumber);
		bool TakeKey(string roomNumber);
		bool ReturnKey(string roomNumber);
		bool DeleteKey(string roomNumber);
		int Save();
	}
}