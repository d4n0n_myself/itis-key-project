﻿using System;
using System.Collections.Generic;
using KeyProject.Core;

namespace KeyProject.Infrastructure.Interfaces
{
	public interface IReservationRepository
	{
		IEnumerable<Reservation> GetReservations();
		IEnumerable<Reservation> GetReservationsByRoom(string roomNumber);
        IEnumerable<Reservation> GetReservationsByDate(DateTime date);
        bool AddReservation(Reservation reservation);
        bool EditReservation(string roomNumber, DateTime reservationStart, Reservation newReservation);
        bool CancelReservation(string roomNumber, DateTime reservationStart);
        int Save();
	}
}
