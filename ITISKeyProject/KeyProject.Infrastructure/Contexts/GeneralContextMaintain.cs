﻿using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace KeyProject.Infrastructure.Context
{
    public class GeneralContextMaintain : IDesignTimeDbContextFactory<GeneralContext>
    {
        public GeneralContext CreateDbContext(string[] args)
        {
            var builder = new ConfigurationBuilder().AddJsonFile(Directory.GetCurrentDirectory() + @"..\appsettings.json");

            var config = builder.Build();
            var connectionString = config.GetConnectionString("DefaultConnection");
            var f = new GeneralContextFactory();
            return f.GetContext(connectionString);
        }
    }
}
