﻿using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace KeyProject.Infrastructure.Context
{
    public class GeneralContextFactory
    {
        public GeneralContext GetContext(string connectionString)
        {
            var options = new DbContextOptionsBuilder<GeneralContext>();
            options.UseSqlServer(connectionString, ob => ob.MigrationsAssembly(typeof(GeneralContext)
                                                           .GetTypeInfo()
                                                           .Assembly
                                                           .GetName()
                                                           .Name));
            return new GeneralContext(options.Options);
        }
    }
}
