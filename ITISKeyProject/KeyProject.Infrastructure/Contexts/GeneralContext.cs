﻿using Microsoft.EntityFrameworkCore;
using KeyProject.Core;

namespace KeyProject.Infrastructure.Context
{
    public class GeneralContext : DbContext
    {
        public GeneralContext(DbContextOptions<GeneralContext> options) : base(options)
        {
            //Database.EnsureCreated();
        }

        public DbSet<Key> Keys { get; set; }

        public DbSet<Identity> Identities { get; set; }

        public DbSet<Reservation> Reservations { get; set; }

        public DbSet<JournalItem> Journal { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}