﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KeyProject.Infrastructure.Migrations
{
    public partial class AddedInStockpropinKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FullName",
                table: "Keys");

            migrationBuilder.AddColumn<bool>(
                name: "InStock",
                table: "Keys",
                nullable: false,
                defaultValue: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "InStock",
                table: "Keys");

            migrationBuilder.AddColumn<string>(
                name: "FullName",
                table: "Keys",
                nullable: true);
        }
    }
}
