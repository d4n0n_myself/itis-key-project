﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KeyProject.Infrastructure.Migrations
{
    public partial class JournalProps : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<System.DateTime>(
                name: "ActionTime", 
                table: "Journal");

            migrationBuilder.AddColumn<string>(
                name: "ActionInfo", 
                table: "Journal");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ActionTime", 
                table: "Journal");

            migrationBuilder.DropColumn(
                name: "ActionInfo", 
                table: "Journal");
        }
    }
}
