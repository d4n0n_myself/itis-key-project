﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KeyProject.Infrastructure.Migrations
{
    public partial class migration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "KeyId",
                table: "Reservations");

            migrationBuilder.AddColumn<string>(
                name: "RoomNumber",
                table: "Reservations",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RoomNumber",
                table: "Reservations");

            migrationBuilder.AddColumn<int>(
                name: "KeyId",
                table: "Reservations",
                nullable: false,
                defaultValue: 0);
        }
    }
}
