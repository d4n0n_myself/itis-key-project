﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KeyProject.Infrastructure.Migrations
{
    public partial class RemovedResEnd : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ReservationEnd", 
                table: "Reservations");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<System.DateTime>(
                name: "ReservationEnd", 
                table: "Reservations");
        }
    }
}
