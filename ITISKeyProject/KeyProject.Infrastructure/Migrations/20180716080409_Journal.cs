﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KeyProject.Infrastructure.Migrations
{
    public partial class Journal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Journal",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)

                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Journal", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Journal");
        }
    }
}
