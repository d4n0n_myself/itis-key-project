﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KeyProject.Infrastructure.Migrations
{
    public partial class AddedRoomNumberInJournal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "RoomNumber",
                table: "Journal",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RoomNumber",
                table: "Journal");
        }
    }
}
