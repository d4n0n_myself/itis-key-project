﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using KeyProject.Core;
using KeyProject.Infrastructure.Context;
using KeyProject.Infrastructure.Interfaces;

namespace KeyProject.Infrastructure.Repos
{
	public class ReservationRepository : IReservationRepository
	{
		private readonly GeneralContext context;

		public ReservationRepository(GeneralContext givenContext)
		{
			context = givenContext;
		}

		public IEnumerable<Reservation> GetReservations()
		{
			return context.Reservations;
		}

        public IEnumerable<Reservation> GetReservationsByDate(DateTime date)
        {
            return context.Reservations.Where(reservation => reservation.ReservationStart.Date == date.Date);
        }

        public IEnumerable<Reservation> GetReservationsByRoom(string roomNumber)
		{
			return context.Reservations.Where(x => x.RoomNumber == roomNumber);
        }

        public bool AddReservation(Reservation reservation)
        {
            if (!CanBeAdded(reservation))
                return false;
            context.Reservations.Add(reservation);
            return true;
        }

        public bool EditReservation(string roomNumber, DateTime reservationStart, Reservation newReservation)
        {
            var reservationInDb = FindReservation(roomNumber, reservationStart);
            if (reservationInDb == null || !CanBeAdded(newReservation, reservationInDb))
                return false;

            reservationInDb.AllowedUsersIDs = newReservation.AllowedUsersIDs;
            reservationInDb.Duration = newReservation.Duration;
            reservationInDb.ReservationStart = newReservation.ReservationStart;
            reservationInDb.RoomNumber = newReservation.RoomNumber;
            return true;
        }

        public bool CancelReservation(string roomNumber, DateTime reservationStart)
        {
            var reservationInDb = FindReservation(roomNumber, reservationStart);
            if (reservationInDb == null)
                return false;

            context.Reservations.Remove(reservationInDb);
            return true;
        }

        public int Save()
		{
			return context.SaveChanges();
		}

        private Reservation FindReservation(string roomNumber, DateTime start)
        {
            return context.Reservations.SingleOrDefault(
                x => x.RoomNumber == roomNumber && x.ReservationStart == start);
        }

        private bool CanBeAdded(Reservation reservation, Reservation excluded = null)
        {
            if (reservation.ReservationStart <= DateTime.Now
                || reservation.ReservationEnd >= reservation.ReservationStart.Date.AddDays(1)
                || !context.Keys.Any(x => x.RoomNumber == reservation.RoomNumber))
                return false;
            
            var reservations = GetReservationsByRoom(reservation.RoomNumber);
            if (excluded != null)
                reservations = reservations.Where(x => x != excluded);
            return !reservations.Any(x => Reservation.AreIntersecting(x, reservation));
        }
	}
}
