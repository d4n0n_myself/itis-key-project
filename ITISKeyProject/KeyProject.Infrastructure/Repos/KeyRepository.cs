﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using KeyProject.Core;
using KeyProject.Infrastructure.Context;
using KeyProject.Infrastructure.Interfaces;

namespace KeyProject.Infrastructure.Repos
{
    public class KeyRepository : IKeyRepository
    {
        private readonly GeneralContext context;

        public KeyRepository(GeneralContext givenContext)
        {
            context = givenContext;
        }

        public IEnumerable<Key> GetKeys()
        {
            return context.Keys.ToList();
        }

        public Key GetKeyByRoomNumber(string roomNumber)
        {
            return context.Keys.SingleOrDefault(x => x.RoomNumber == roomNumber);
        }

        public bool AddKey(string roomNumber)
        {
            if (GetKeyByRoomNumber(roomNumber) != null)
                return false;
            context.Keys.Add(new Key(roomNumber));
            return true;
        }

        public bool TakeKey(string roomNumber)
        {
            var key = GetKeyByRoomNumber(roomNumber);
            if (key == null || !key.InStock)
                return false;

            var now = DateTime.Now;
            if (context.Reservations.Any(x => x.RoomNumber == roomNumber && x.Includes(now)))
                return false;
            key.InStock = false;
            return true;
        }

        public bool ReturnKey(string roomNumber)
        {
            var key = GetKeyByRoomNumber(roomNumber);
            if (key == null || key.InStock)
                return false;

            key.InStock = true;
            return true;
        }

        public bool DeleteKey(string roomNumber)
        {
            var key = GetKeyByRoomNumber(roomNumber);
            if (key == null)
                return false;

            context.Keys.Remove(GetKeyByRoomNumber(roomNumber));
            context.Reservations.RemoveRange(context.Reservations.Where(x => x.RoomNumber == roomNumber));
            return true;
        }

        public int Save()
        {
            return context.SaveChanges();
        }
    }
}