﻿using System;
using System.Collections.Generic;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using KeyProject.Core;
using KeyProject.Infrastructure.Context;
using KeyProject.Infrastructure.Interfaces;

namespace KeyProject.Infrastructure.Repos
{
    public class JournalRepository : IJournalRepository
    {
        private readonly GeneralContext context;

        public JournalRepository(GeneralContext givenContext)
        {
            context = givenContext;
        }

        public void AddRecord(Key key, DateTime dateTime, string action)
        { 
             context.Journal.Add(new JournalItem(dateTime, key, action));
        }

        public IEnumerable<JournalItem> GetJournal()
        {
            return context.Journal;
        }

        public IEnumerable<JournalItem> GetJournalByDate(DateTime date)
        {
            return context.Journal.Where(item => date.Date == item.ActionTime);
        }

        public int Save()
        {
            return context.SaveChanges();
        }
    }
}
