﻿using System;
using System.Threading.Tasks;
using KeyProject.Core;
using KeyProject.Infrastructure.Context;
using KeyProject.Infrastructure.Interfaces;

namespace KeyProject.Infrastructure.Repos
{
    public class IdentityRepository : IIdentityRepository
    {
        private readonly GeneralContext context;

        public IdentityRepository(GeneralContext givenContext)
        {
            context = givenContext;
        }

        public async Task AddIdentity(Identity identity)
        {
            await context.AddAsync(identity);
        }

        public int Save()
        {
            return context.SaveChanges();
        }
    }
}
